package com.example.demo.service;

import com.example.demo.repository.HttpEn;
import com.example.demo.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository = new UserRepository();

    public HttpEn checkLogin(String login, String password) {
        return this.userRepository.checkLogin(login, password);
    }

}
