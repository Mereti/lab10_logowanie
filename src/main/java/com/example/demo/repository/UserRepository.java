package com.example.demo.repository;

import com.example.demo.model.User;
import java.util.HashMap;
import java.util.Map;


public class UserRepository {

    private final Map<Integer, User> usersDatabase;
    private User user;
    private static final int REPEAT =  3;
    private HttpEn httpEn;


    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public HttpEn checkLogin(final String login, final String password) {

        for(User user : usersDatabase.values()) {
            if (user.getLogin().equals(login)) {
                if(user.isActive()) {
                    if (!user.getPassword().equals(password)) {
                        int number = user.getIncorrectLoginCounter();
                        number+=1;
                        user.setIncorrectLoginCounter(number);
                        if (user.getIncorrectLoginCounter() >= REPEAT){
                            user.setActive(false);
                            return HttpEn.FORBIDDEN;
                        }
                        return HttpEn.UNAUTHORIZED;
                    }else {
                        return HttpEn.OK;
                    }
                }else return HttpEn.FORBIDDEN;
            }
        }
        return  HttpEn.UNAUTHORIZED;
    }
}
