package com.example.demo.repository;

public enum HttpEn {
    OK(200),
    FORBIDDEN(403),
    UNAUTHORIZED(401);

    private int value;

    HttpEn(int i) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
